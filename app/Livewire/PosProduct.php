<?php

namespace App\Livewire;

use Livewire\Component;
use App\Services\Ecommerce\EcommerceService;

class PosProduct extends Component
{
    public $carts,$categoryInput = [],$search, $paginate;

    // public $isOpen = 0;

    // public function create()
    // {
    //     $this->openModal();
    // }
    // public function openModal()
    // {
    //     $this->isOpen = true;
    // }
    // public function closeModal()
    // {
    //     $this->isOpen = false;
    // }

    protected $updateQuerystring =['search'];
    public function mount() {
        $this->search = request()->query('search',$this->search);
    }

    public function render() {
        if (auth()->user()) {
            $user_id = auth()->user()->id;
        } else {
            $user_id = 0;
        }

        if ($this->search) {
            $products = EcommerceService::getProductBySearch($this->search,
                $user_id);
        } else {
            //get product
            $products = EcommerceService::getProduct($user_id);
        }

        $categories = EcommerceService::getCategory();


        $products = collect($products);

        // dd($products);
        // dd($this->search);


        return view('livewire.pos-product',[
            'products' => $products,
            'categories' => $categories,
        ]);
    }

    //addItem openModal pos-product-modal
    public function addItem($product_sku) {
        $user_id = auth()->user()->id;

        $addCart = EcommerceService::addToCart($user_id,$product_sku);
        $this->dispatch('cart_updated');
    }

}
