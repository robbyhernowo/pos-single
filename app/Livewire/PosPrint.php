<?php

namespace App\Livewire;

use Livewire\Component;
use App\Services\Ecommerce\EcommerceService;

class PosPrint extends Component
{
    public $newOrder, $salesorder,$salesorderDetail, $postPayment,$sumPayment;
    public function render()
    {
        $user_id = auth()->user()->id;

        $this->salesorder = EcommerceService::getSales($this->newOrder);
        $this->salesorderDetail = EcommerceService::getSalesDetail($this->newOrder);
        $this->postPayment = EcommerceService::getPaymentSalesIn($this->newOrder);
        $this->postPayment = collect($this->postPayment);
        $this->sumPayment = $this->salesorder[0]->total - ($this->postPayment->sum('amount') ?? 0);
        // dd($this->salesorder);

        //update status 8 complete
        $status = 8;
        EcommerceService::updateStatus($this->newOrder, $status, $user_id);

        return view('livewire.pos-print');
    }
}
