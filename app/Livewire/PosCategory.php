<?php

namespace App\Livewire;

use Livewire\Component;
use App\Services\Ecommerce\EcommerceService;

class PosCategory extends Component
{
    public $categories;
    public function render()
    {
        if (auth()->user()) {
            $user_id = auth()->user()->id;
        } else {
            $user_id = 0;
        }

        $this->categories = EcommerceService::getCategory();
        // dd($this->categories);
        return view('livewire.pos-category');
    }
}
