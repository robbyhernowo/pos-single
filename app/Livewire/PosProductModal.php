<?php

namespace App\Livewire;

use Livewire\Component;

class PosProductModal extends Component
{
    protected $listeners = ['openModal' => 'showModal'];
    public $showModal = false;

    public function openModal()
    {
        $this->showModal = true;
    }

    public function hideModal()
    {
        $this->showModal = false;
    }

    public function render()
    {
        return view('livewire.pos-product-modal');
    }
}
