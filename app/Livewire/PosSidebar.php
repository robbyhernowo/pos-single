<?php

namespace App\Livewire;

use Livewire\Component;
use App\Services\Ecommerce\EcommerceService;
use App\Models\pos_sales_payment;

class PosSidebar extends Component
{
    public $alert;
    public $orderHistory,$orderHistoryCount,$products, $categories, $carts,$cartsCount,$cartsTotal, $quantities = [], $newOrder,$salesorder,$salesorderDetail, $totalOrder;
    public $showModal = false;
    public $paymentType, $payment_type_id, $reference_no, $amount, $sendPaymentType;
    //discount peritem
    public $discountTypes= [],$discounts = [];
    //manual payment
    public $i = 1;
    public $inputs = [],$payment_type_id_manual = [],$amount_manual = [], $postPayment, $sumPayment;
    // protected $listeners = ['cart_updated' => 'render'];
    //cart_update => render, payment_update => getPayment
    protected $listeners = ['cart_updated' => 'render', 'payment_update' => 'getPayment'];

    protected $rules = [
        'sendPaymentType' => 'required'
    ];

    public $isOpen = 0;

    private function resetInputFields(){
        $this->payment_type_id = '';
        $this->amount = '';
        $this->payment_type_id_manual = [];
        $this->amount_manual = [];
        $this->discountTypes = [];
        $this->discounts = [];
    }

    public function openModal($SoNumber) {
        // $SoNumber = 'SOBP2230900003';
        $this->paymentType = EcommerceService::getPaymentType();
        $this->salesorder = EcommerceService::getSales($SoNumber);
        $this->salesorderDetail = EcommerceService::getSalesDetail($SoNumber);
        $this->isOpen = true;
        $this->newOrder = $SoNumber;
        $this->sumPayment = $this->salesorder[0]->total;
        // dd($this->salesorder)

    }

    public function closeModal() {
        $this->isOpen = false;

        $this->resetInputFields();
    }

    public function render() {
        $user_id = auth()->user()->id;

        $this->orderHistory = EcommerceService::getOrderHistoryToday($user_id);
        //count orderHistory
        $this->orderHistoryCount = count($this->orderHistory);
        $this->products = EcommerceService::getProduct($user_id);
        $this->categories = EcommerceService::getCategory();
        $this->carts = EcommerceService::getCart($user_id);
        $this->cartsCount = count($this->carts);

        //  dd($this->orderHistoryCount);

        $this->cartsTotal = 0;
        foreach ($this->carts as $cart) {
            // $this->cartsTotal += $cart->qty * $cart->unit_price;

            //if has $cart->discount_percent (($cart->qty * $cart->unit_price * (100 - $cart->discount_percent)) / 100, 0, ',', '.')
            if ($cart->discount_percent != null) {
                $this->cartsTotal += ($cart->qty * $cart->unit_price * (100 - $cart->discount_percent)) / 100;
            } elseif ($cart->discount_amount != null) {
                $this->cartsTotal += ($cart->qty * $cart->unit_price) - $cart->discount_amount;
            } else {
                $this->cartsTotal += $cart->qty * $cart->unit_price;
            }
        }

        // dd($this->carts);

        //check if $this->sendPaymentType not empty
        if ($this->sendPaymentType != null) {
            $this->validate();
            pos_sales_payment::create([
                'payment_type_id' => $this->sendPaymentType,
                'amount' => $this->totalOrder ,
                'reference_no' =>  '',
                'sales_no' => $this->newOrder,
                'created_by' => auth()->user()->id,
            ]);
        }
        return view('livewire.pos-sidebar');
    }

    public function decrementQuantity($sku) {
        $user_id = auth()->user()->id;
        $cart = EcommerceService::getCartBySku($sku, $user_id);
        //convert array to object
        $cart = json_decode(json_encode($cart), FALSE);
        // dd($cart);
        $cart = $cart[0];
        $cart->qty = $cart->qty - 1;
        //check if minus
        if ($cart->qty < 1) {
            // $cart->qty = 1;
            //remove sku from cart
            $removeCart = EcommerceService::removeCart($cart->sku, $user_id);
        }
        //update qty
        $updateQty = EcommerceService::updateQty(
            $cart->sku,
            $user_id,
            $cart->qty
        );
    }

    public function incrementQuantity($sku) {
        $user_id = auth()->user()->id;
        $cart = EcommerceService::getCartBySku($sku, $user_id);
        //convert array to object
        $cart = json_decode(json_encode($cart), FALSE);
        $cart = $cart[0];
        // $cart->sku = $cart->sku
        // dd($cart);
        $cart->qty = $cart->qty + 1;
        //update qty
        $updateQty = EcommerceService::updateQty(
            $cart->sku,
            $user_id,
            $cart->qty
        );
        // dd($updateQty);
    }

    public function updateItem($sku) {
        $user_id = auth()->user()->id;
        $cart = EcommerceService::getCartBySku($sku, $user_id);
        //convert array to object
        $cart = json_decode(json_encode($cart), FALSE);
        // dd($cart);
        $cart = $cart[0];
        // $cart->qty = $this->quantities;
        //check $this->quantities[$sku] if has , change to .
        if (strpos($this->quantities[$sku], ',') !== false) {
            $this->quantities[$sku] = str_replace(',', '.', $this->quantities[$sku]);
        }
        //get value from quantities $sku
        $cart->qty = $this->quantities[$sku];
        //update qty
        $updateQty = EcommerceService::updateQty(
            $cart->sku,
            $user_id,
            $cart->qty
        );
        // $this->carts = EcommerceService::getCart($user_id);

        $this->dispatch('cart_updated');
    }

    public function removeItem($sku){
        $user_id = auth()->user()->id;
        $remove = EcommerceService::removeCartItem($sku, $user_id);
        $this->carts = EcommerceService::getCart($user_id);

        $this->dispatch('cart_updated');
    }

    public function submitOrder(){
        // $this->validate([
        //     'sendPaymentType' => 'required'
        // ]);
        $user_id = auth()->user()->id;
        $status = 1;
        $fulfill_date = date('Y-m-d H:i:s');
        $note = "Offline Store";


        $selectedItem = EcommerceService::getCartItem($user_id);
        $selectedItem = json_decode(json_encode($selectedItem), true);

        //validate selectedItem
        if ($selectedItem == null) {
            // $this->dispatch('alert', ['message' => 'Please add item first']);
            // dd($this->alert);
            // return;

            //dispatch to variable $alert
            $this->alert = [
                'type' => 'danger',
                'message' => 'Pilih Item Dulu',
            ];
            // dd($this->alert);
            return;
        }

        $getData = EcommerceService::GetClientNoNChannel($user_id);
        $getData = json_decode(json_encode($getData), true);
        $client_no = $getData[0]['client_no'];
        $channel_id = $getData[0]['channel_id'];

        $getData = EcommerceService::GetClientNoNChannel($user_id);
        $getData = json_decode(json_encode($getData), true);
        $client_no = $getData[0]['client_no'];
        $channel_id = $getData[0]['channel_id'];

        $CheckSo = EcommerceService::CheckSo($channel_id);
        $CheckSo = json_decode(json_encode($CheckSo), true);
        $CheckSo = $CheckSo[0]["no"];

        $ChannelCode = EcommerceService::ChannelCode($client_no);
        $ChannelCode = json_decode(json_encode($ChannelCode), true);
        $ChannelCode = $ChannelCode[0]['code'];

        if ($CheckSo == null) {
            $SoNumber = 'SO' . $ChannelCode . date('ym') . '00001';
        } else if ($CheckSo != null) {
            //explode last 5 digit Checkso
            $last5digit = substr($CheckSo, -5);
            //increment last 5 digit
            $increment = $last5digit + 1;
            //if $increment length 1 digit + 0000
            if (strlen($increment) == 1) {
                $increment = '0000' . $increment;
            } else if (strlen($increment) == 2) {
                $increment = '000' . $increment;
            } else if (strlen($increment) == 3) {
                $increment = '00' . $increment;
            } else if (strlen($increment) == 4) {
                $increment = '0' . $increment;
            } else {
                $increment = $increment;
            }
            //generate So Number
            $SoNumber = 'SO' . $ChannelCode . date('ym') . $increment;
        } else if (strpos($CheckSo, '-') !== false) {
            $explode = explode('-', $CheckSo);
            $last5digit = substr($explode[1], -5);
            //increment last 5 digit
            $increment = $last5digit + 1;
            //if $increment length 1 digit + 0000
            if (strlen($increment) == 1) {
                $increment = '0000' . $increment;
            } else if (strlen($increment) == 2) {
                $increment = '000' . $increment;
            } else if (strlen($increment) == 3) {
                $increment = '00' . $increment;
            } else if (strlen($increment) == 4) {
                $increment = '0' . $increment;
            } else {
                $increment = $increment;
            }
            //generate So Number
            $SoNumber = 'SO' . $ChannelCode . date('ym') . $increment;
        }

        $this->totalOrder = 0;

        foreach ($selectedItem as $key => $value) {
            // $total += $value['unit_price'] * $value['qty'];

             //total_final = (unit_price * qty_order) + ((unit_price * qty_order) * tax_percent) /100
            // $total += ($value['unit_price'] * $value['qty']) + (($value['unit_price'] * $value['qty']) * $value['tax_percent']) / 100;

            //if has $cart->discount_percent (($cart->qty * $cart->unit_price * (100 - $cart->discount_percent)) / 100, 0, ',', '.')
            if ($value['discount_percent'] != null) {
               $this->totalOrder  += ($value['unit_price'] * $value['qty'] * (100 - $value['discount_percent'])) / 100;
            } elseif ($value['discount_amount'] != null) {
               $this->totalOrder  += ($value['unit_price'] * $value['qty']) - $value['discount_amount'];
            } else {
               $this->totalOrder  += $value['unit_price'] * $value['qty'];
            }
        }

        $dataHeader = EcommerceService::SaveHeader($SoNumber,$channel_id,$client_no, $fulfill_date, $status,$this->totalOrder ,$note, $user_id );
        foreach ($selectedItem as $key => $value) {
            //count total price unit_price * qty
            // $totalPrice = $value['unit_price'] * $value['qty'];
              //if has $cart->discount_percent (($cart->qty * $cart->unit_price * (100 - $cart->discount_percent)) / 100, 0, ',', '.')

              $totalPrice = $value['unit_price'] * $value['qty'];

            if ($value['discount_percent'] != null) {
                $totalFinal = ($value['unit_price'] * $value['qty'] * (100 - $value['discount_percent'])) / 100;
            } elseif ($value['discount_amount'] != null) {
                $totalFinal = ($value['unit_price'] * $value['qty']) - $value['discount_amount'];
            } else {
                $totalFinal = $value['unit_price'] * $value['qty'];
            }
            //count total final
            //datenow
            $created_at = date('Y-m-d H:i:s');

            //replace $value['discount_percent'] dot with coma
            // $value['discount_percent'] = str_replace('.', ',', $value['discount_percent']);

            //check if $value['discount_percent'] =  null set 0
            if ($value['discount_percent'] == null) {
                $value['discount_percent'] = 0;
            } else {
                $value['discount_percent'] = $value['discount_percent'];
            }

            if ($value['discount_amount'] == null) {
                $value['discount_amount'] = 0;
            } else {
                $value['discount_amount'] = $value['discount_amount'];
            }

            $taxAmount = 0;

            $dataDetail = EcommerceService::SaveDetail(
                    $SoNumber,
                    $value['sku'],
                    $value['unit_price'],
                    $value['qty'],
                    $totalPrice ,
                    $value['tax_percent'],
                    $value['discount_amount'],
                    $value['discount_percent'],
                    $taxAmount,
                    $totalFinal,
                    $user_id,
                    $created_at);
        }

        $delete = EcommerceService::deleteCart($user_id);

        $isOpen = true;
        $this->dispatch('cart_updated');

        //function openModal with $SoNumber
        $this->openModal($SoNumber);
    }

    public function choosePayment($payment_id){
        $this->sendPaymentType = $payment_id;
        pos_sales_payment::create([
            'payment_type_id' => $this->sendPaymentType,
            'amount' => $this->totalOrder ,
            'reference_no' =>  '',
            'sales_no' => $this->newOrder,
            'created_by' => auth()->user()->id,
        ]);
    }

    public function addPayment($i){
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }

    public function add($i){
        // array_push($this->inputs, [
        //     'payment_type_id_manual' => '',
        //     'amount_manual' => '',
        // ]);
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }

    public function removePayment($i){
        // unset($this->inputs[$i]);
        //foreach $this->payment_type_id_manual delete from pos_sales_payment
        // foreach ($this->payment_type_id_manual as $key => $value) {
        //     $delete = EcommerceService::deletePayment($this->newOrder, $this->payment_type_id_manual[$key]);
        //     // unset
        //     unset($this->payment_type_id_manual[$key]);
        // }

      //unset inputs key and delete search payment_type_id from key
        // unset($this->inputs[$i]);
        // unset($this->payment_type_id_manual[$i]);
        // unset($this->amount_manual[$i]);

        // dd($this->payment_type_id_manual);
        // dd($i);
        //get id from $i
        $id = $this->payment_type_id_manual[$i];
        // dd($id);
        //delete payment_type_id from pos_sales_payment
        $delete = EcommerceService::deletePayment($this->newOrder, $id);
        unset($this->inputs[$i]);

        $this->dispatch('payment_update');

    }

    public function countManualPayment(){
        // dd($this->sendPaymentType);
            // pos_sales_payment::create([
            //     'payment_type_id' => $this->sendPaymentType,
            //     'amount' => $this->totalOrder ,
            //     'reference_no' =>  '',
            //     'sales_no' => $this->newOrder,
            //     'created_by' => auth()->user()->id,
            // ]);

            // dd($this->amount_manual);
        $this->validate([
            'payment_type_id_manual' => 'required',
            'amount_manual' => 'required',
        ]);

        // foreach ($this->payment_type_id_manual as $key => $value) {
        //     pos_sales_payment::create([
        //         'payment_type_id' => $this->payment_type_id_manual[$key],
        //         'amount' => $this->amount_manual[$key],
        //         'sales_no' => $this->newOrder,
        //         //created_by
        //         'created_by' => auth()->user()->id,
        //     ]);
        // }
        foreach ($this->payment_type_id_manual as $key => $value) {
            pos_sales_payment::updateOrInsert(
                [
                    'sales_no' => $this->newOrder,
                    'payment_type_id' => $this->payment_type_id_manual[$key],
                ],
                [
                    'amount' => $this->amount_manual[$key],
                    'created_by' => auth()->user()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                ]
            );
        }

        $this->dispatch('payment_update');
        // $this->closeModal();
        // return redirect()->route('posPrint', ['newOrder' => $this->newOrder]);
        // return redirect()->route('posPrint', ['newOrder' => $this->newOrder])->with('openInNewTab', true);



    }

    public function updatePaymentManual($amount){
        // $this->amount_manual = $amount;
        // dd($this->amount_manual);

        foreach ($this->payment_type_id_manual as $key => $value) {
            pos_sales_payment::updateOrInsert(
                [
                    'sales_no' => $this->newOrder,
                    'payment_type_id' => $this->payment_type_id_manual[$key],
                ],
                [
                    'amount' => $this->amount_manual[$key],
                    'created_by' => auth()->user()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                ]
            );

            $this->dispatch('payment_update');
        }
    }

    public function getPayment(){
        $this->salesorder = EcommerceService::getSales($this->newOrder);
        $this->postPayment = EcommerceService::getPaymentSalesIn($this->newOrder);
        $this->postPayment = collect($this->postPayment);
        $this->sumPayment = $this->salesorder[0]->total - ($this->postPayment->sum('amount') ?? $this->salesorder[0]->total);

        // dd($this->sumPayment);
    }

    public function updateDiscount($sku){
        // dd($this->salesorderDetail);

        $hasDetail = EcommerceService::hasDetail($this->newOrder, $sku);
        $hasDetail = json_decode(json_encode($hasDetail), true);
        $hasDetail = $hasDetail[0];

        $detaildiscount = $this->discounts[$sku];
        $detaildiscountTypes = $this->discountTypes[$sku];

        // dd($detaildiscountTypes);
        // dd($this->discountTypes);
        //change .
        if (strpos($this->discounts[$sku], ',') !== false) {
            $this->discounts[$sku] = str_replace(',', '.', $this->discounts[$sku]);
        }

        //check if discountTypes = percent
        if ($detaildiscountTypes == 'percent') {
            //get amount
            $amount = $hasDetail['total_price'] * $detaildiscount / 100;
            //get percent
            $percent = $detaildiscount;
            //update Salesdetail
            $updateSalesDetail = EcommerceService::updateSalesDetail($this->newOrder, $sku, $percent, $amount);

        $this->dispatch('payment_update');
        } elseif ($detaildiscountTypes == 'amount'){
            //get amount
            $amount = $detaildiscount;
            //get percent
            $percent = $amount / $hasDetail['total_price'] * 100;
            //update Salesdetail
            $updateSalesDetail = EcommerceService::updateSalesDetail($this->newOrder, $sku, $percent, $amount);

        $this->dispatch('payment_update');
        } else {
            $amount = 0;
        }

        $this->dispatch('payment_update');
        // dd($percent);

    }
}
