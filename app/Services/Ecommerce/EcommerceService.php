<?php

namespace App\Services\Ecommerce;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

class EcommerceService
{
    //orderhistory today
    public static function getOrderHistoryToday($user_id) {
        $sql = "SELECT a.no as invoice,
        a.total,
        a.status,
        a.created_at
         FROM sales a
         WHERE a.created_at >= current_date()
         AND a.created_by = '$user_id'
         ORDER BY a.created_at DESC
         limit 50";
        return DB::select($sql);
    }

    public static function getSalesOrder($like) {
        $sql = "SELECT a.no as invoice,
        a.total,
        a.status,
        a.created_at
         FROM sales a
         WHERE a.no LIKE '%$like%'
         ORDER BY a.created_at DESC
         limit 50";
        return DB::select($sql);
    }

    public static function updateStatus($order_id, $status_void,$user_id) {
        $sql = "UPDATE sales SET status = '$status_void',
        updated_at = NOW(),
        updated_by = '$user_id'
        WHERE no = '$order_id'";
        return DB::update($sql);
    }

    public static function getPaymentSalesIn($order_id) {
        $sql = "SELECT * FROM pos_sales_payment a
        LEFT JOIN master_payment_type b ON b.id = a.payment_type_id
        WHERE a.sales_no = '$order_id'
        AND a.payment_type_id <> 10";
        return DB::select($sql);
    }

    public static function getPaymentType() {
        $sql = "SELECT * FROM master_payment_type";
        return DB::select($sql);
    }

    public static function getSalesPayment() {
        $sql = "SELECT * FROM pos_sales_payment";
        return DB::select($sql);
    }

    //deleteCart
    public static function deleteCart($user_id) {
        $sqlDelete = "
        DELETE FROM pos_cart
        WHERE user_id = '$user_id'
        ";

        DB::beginTransaction();
        DB::delete($sqlDelete);
        DB::commit();
    }
    public static function updateOrdermidtransCallback($no, $status) {
        $sqlUpdate = "
        UPDATE sales a
        SET a.status = '$status'
        WHERE a.no = '$no'
        ";

        DB::beginTransaction();
        DB::insert($sqlUpdate);
        DB::commit();
    }

    public static function getOrderCallback($no) {
        $sql = "SELECT * FROM sales WHERE no = '$no'";
    }

    public static function getSales($soNumber) {
        $sql = "SELECT no,
        total,
        status,
        created_at
         FROM sales WHERE no = '$soNumber'";
        return DB::select($sql);
    }

    public static function getSalesDetail($soNumber) {
        $sql = "SELECT a.sku,
        b.name,
                -- a.qty_order,
                CASE
                 WHEN ROUND(a.qty_order,3) <> ROUND(a.qty_order,0) THEN a.qty_order
                    ELSE ROUND(a.qty_order,0)
                END as qty_order,
                a.unit_price,
                a.total_price,
                a.tax_percent,
                a.tax_amount,
                a.discount_amount,
                a.discount_percent,
                a.total_final
                 FROM sales_detail a
                        LEFT JOIN master_item b ON a.sku = b.sku
                            WHERE a.sales_no = '$soNumber'";
        return DB::select($sql);
    }


    public static function SaveDetail($SoNumber,
    $sku,
    $unit_price,
    $qty,
    $totalPrice,
    $tax_percent,
    $discount_amount,
    $discount_percent,
    $taxAmount,
    $totalFinal,
    $created_by,
    $created_at
    ) {

            $sql = "INSERT INTO sales_detail (sales_no, sku, unit_price, qty_order, total_price,total_amount, tax_percent,discount_amount,discount_percent, tax_amount, total_final, created_by, created_at, updated_by, updated_at)
            VALUES ('$SoNumber', '$sku', '$unit_price', '$qty', '$totalPrice','$totalPrice', '$tax_percent','$discount_amount','$discount_percent', '$taxAmount', '$totalFinal', '$created_by', '$created_at', '$created_by', '$created_at')";

            $sqlUpdate = "UPDATE sales a
            SET a.total = ( select sum(x.total_final) from sales_detail x where x.sales_no = a.no)
            WHERE a.no = '$SoNumber'";

            DB::beginTransaction();
            DB::insert($sql);
            DB::update($sqlUpdate);
            DB::commit();

            //return data sql
            return $sql;

    }


    public static function CheckSo($channel_id) {


        $sql = "SELECT max(no) as no from sales a where channel_id = $channel_id and MONTH(date) = MONTH(current_date()) AND a.no NOT LIKE '%CH%'";

        return DB::select($sql);
    }

    public static function GetClientNoNChannel($user_id) {


        $sql = "SELECT  b.channel_id, a.pos_client_no as client_no
        FROM users a, master_client b
        WHERE b.no = a.pos_client_no
        AND a.id = $user_id";

        return DB::select($sql);
    }

    public static function SaveHeader($SoNumber,$channel_id,$client_no, $fulfill_date,$status,$total,$note, $created_by) {

        $sql = "INSERT INTO sales (no, no_accurate, channel_id,client_no, date, fulfill_date, status, total, note, created_by, created_at, updated_by, updated_at)
        VALUES ('$SoNumber','$SoNumber', $channel_id,$client_no, current_date(), '$fulfill_date', '$status', '$total', '$note', '$created_by', current_timestamp(), '$created_by', current_timestamp())";

            DB::beginTransaction();
            DB::insert($sql);
            DB::commit();

            //return data sql
            return $sql;

    }

    public static function ChannelCode($client_no) {
        $sql = "SELECT c.code
        FROM master_client a, master_channel b, master_depo c
        WHERE a.channel_id = b.id and b.depo_id = c.id
        and a.no = '$client_no'";

        return DB::select($sql);
    }
    public static function GetChannelId($user_id) {
        $sql = "SELECT b.channel_id
        FROM users a
        LEFT JOIN user_channel b ON b.user_id = a.id
        WHERE a.id = '$user_id'";

        return DB::select($sql);
    }

    public static function getCartItem($user_id) {
        $sql = "SELECT a.sku, b.name, a.qty, a.discount_amount, a.discount_percent,
        (select x.price from master_item_price x
        where x.period_end >= current_date() and x.group_price_id = d.client_group_price_id
        and
        x.sku = a.sku order by updated_at desc limit 1
        ) as unit_price,
        0 as tax_percent
        FROM pos_cart a
        LEFT JOIN master_item b ON b.sku = a.sku
        LEFT JOIN users c ON c.id = a.user_id
        LEFT JOIN master_client d ON d.no = c.pos_client_no
        WHERE a.user_id = $user_id";
        return DB::select($sql);
    }

    //getSalesOrder
    // public static function getSalesOrder(
    //     $user_id,
    //     $channel_id,
    //     $client_no,
    //     $status
    // ) {
    //     $url = env('BACKEND_API_URL') . '/v1/sales/b2c/checkout/create';
    //     $response = Http::withToken(env('ACCESS_TOKEN'))->post($url, [
    //         'channel_id' => $channel_id,
    //         'client_no' => $client_no,
    //         'created_by' => $user_id,
    //         'status' => $status
    //     ]);

    //     // // log $response pass to api
    //     Log::info('DATAGetSalesOrder: ' . json_encode([
    //         'user_id' => $user_id,
    //         'channel_id' => $channel_id,
    //         'client_no' => $client_no,
    //         'status' => $status
    //     ]));
    //     return $response->object();
    // }

    public static function getCart($user_id){
        $sql = "SELECT a.sku, b.name, a.qty, a.discount_amount, a.discount_percent,
        (select x.price from master_item_price x
        where x.period_end >= current_date() and x.group_price_id = d.client_group_price_id
        and
        x.sku = a.sku order by updated_at desc limit 1
        ) as unit_price,
        CONCAT('https://freshbox-test1.s3.ap-southeast-3.amazonaws.com/photo_item/', e.photo) as photo
        FROM pos_cart a
        LEFT JOIN master_item b ON b.sku = a.sku
        LEFT JOIN users c ON c.id = a.user_id
        LEFT JOIN master_client d ON d.no = c.pos_client_no
        LEFT JOIN master_item_gallery e ON e.sku = a.sku
        WHERE a.user_id = $user_id
        ORDER BY a.id desc";

        return DB::select($sql);
    }
    //remove removeCartItem
    public static function removeCartItem($sku, $user_id){
        $sql = "DELETE FROM pos_cart WHERE user_id = $user_id and sku = '$sku';";

        return DB::select($sql);
    }

    //removeDiscount
    public static function removeDiscount($sku, $user_id){
        $sql = "UPDATE pos_cart SET discount_amount = null, discount_percent = null WHERE user_id = $user_id and sku = '$sku';";

        return DB::select($sql);
    }

    //updateqty in cart
    public static function updateQty($sku, $user_id, $qty){
        $sql = "UPDATE pos_cart SET qty = $qty WHERE user_id = $user_id and sku = '$sku';";

        return DB::select($sql);
    }

    //remove cart
    public static function removeCart($sku,$user_id){
        $sql = "DELETE FROM pos_cart WHERE user_id = $user_id and sku = '$sku';";

        return DB::select($sql);
    }

    //updateDiscountAmount
    public static function updateDiscountAmount($sku, $user_id, $discount){
        $sql = "UPDATE pos_cart SET discount_amount = $discount WHERE user_id = $user_id and sku = '$sku';";

        return DB::select($sql);
    }

    //updateDiscountPercent
    public static function updateDiscountPercent($sku, $user_id, $discount){
        $sql = "UPDATE pos_cart SET discount_percent = $discount WHERE user_id = $user_id and sku = '$sku';";

        return DB::select($sql);
    }

    //getCartBySku
    public static function getCartBySku($sku, $user_id){
        $sql = "SELECT a.sku, b.name, a.qty,
        (select x.price from master_item_price x
        where x.period_end >= current_date() and x.group_price_id = (SELECT c.group_price_id from users a
        LEFT JOIN user_channel b ON b.user_id = a.id
        LEFT JOIN pos_channel c ON c.channel_id = b.channel_id
        WHERE a.id = $user_id
        LIMIT 1)
        and
        x.sku = a.sku order by updated_at desc limit 1
        ) as unit_price
        FROM pos_cart a
        LEFT JOIN master_item b ON b.sku = a.sku
        WHERE a.user_id =$user_id and a.sku = '$sku';";

        return DB::select($sql);
    }

    public static function getProductBySku($sku, $user_id) {
        $sql = "SELECT distinct(a.sku) as sku, b.name, d.name as uom_name, b.description, b.item_category_id, c.name as category_name,
        (select x.price from master_item_price x
        where x.period_end >= current_date() and x.group_price_id = a.group_price_id and
        x.sku = a.sku order by updated_at desc limit 1
        ) as price
        FROM master_item_price a
        LEFT JOIN master_item b ON b.sku = a.sku
        LEFT JOIN master_item_category c ON c.id = b.item_category_id
        LEFT JOIN master_uom d ON d.id = b.uom_id
        WHERE  a.period_end > current_date() and a.group_price_id = (SELECT c.group_price_id from users a
        LEFT JOIN user_channel b ON b.user_id = a.id
        LEFT JOIN pos_channel c ON c.channel_id = b.channel_id
        WHERE a.id = $user_id
        LIMIT 1)
        and a.sku = '$sku'";

        return DB::select($sql);
    }

    public static function addToCart($user_id, $product_sku){
        $sql = "INSERT INTO pos_cart (user_id, sku, qty, timestamp)
        VALUES ($user_id, '$product_sku', 1, now())
        ON DUPLICATE KEY UPDATE qty = qty + 1;";

        return DB::select($sql);
    }

    public static function getProduct($user_id) {
        $sql = "SELECT a.sku,
        b.name,
        d.name as uom_name,
        b.description,
        b.item_category_id,
        c.name as category_name,
                (SELECT x.price
                 FROM master_item_price x
                 WHERE x.period_start <= current_date() and x.period_end >= current_date()
                     AND x.group_price_id = a.group_price_id
                     AND x.sku = a.sku
                 ORDER BY updated_at DESC
                 LIMIT 1) as unit_price,
        CONCAT('https://freshbox-test1.s3.ap-southeast-3.amazonaws.com/photo_item/', e.photo) as photo
        FROM master_item_price a
        LEFT JOIN master_item b ON b.sku = a.sku
        LEFT JOIN master_item_category c ON c.id = b.item_category_id
        LEFT JOIN master_uom d ON d.id = b.uom_id
        LEFT JOIN master_item_gallery e ON e.sku = a.sku
        WHERE a.period_end > current_date()
        AND e.is_main = 1
        AND a.group_price_id = (SELECT y.client_group_price_id FROM users x, master_client y WHERE x.pos_client_no = y.no and x.id = $user_id)";
        // $sql = "SELECT distinct(a.sku) as sku, b.name, d.name as uom_name, b.description, b.item_category_id, c.name as category_name,
        // (select x.price from master_item_price x
        // where x.period_end >= current_date() and x.group_price_id = a.group_price_id and
        // x.sku = a.sku order by updated_at desc limit 1
        // ) as unit_price,
        // a.sku as photo
        // FROM master_item_price a
        // LEFT JOIN master_item b ON b.sku = a.sku
        // LEFT JOIN master_item_category c ON c.id = b.item_category_id
        // LEFT JOIN master_uom d ON d.id = b.uom_id
        // WHERE  a.period_end > current_date() and a.group_price_id = (SELECT c.group_price_id from users a
        // LEFT JOIN user_channel b ON b.user_id = a.id
        // LEFT JOIN pos_channel c ON c.channel_id = b.channel_id
        // WHERE a.id = $user_id
        // LIMIT 1)";

        return DB::select($sql);
    }

    public static function getProductByCategory($category_name, $user_id) {
        $sql = "SELECT a.sku,
        b.name,
        d.name as uom_name,
        b.description,
        b.item_category_id,
        c.name as category_name,
                (SELECT x.price
                 FROM master_item_price x
                 WHERE x.period_start <= current_date() and x.period_end >= current_date()
                     AND x.group_price_id = a.group_price_id
                     AND x.sku = a.sku
                 ORDER BY updated_at DESC
                 LIMIT 1) as unit_price,
        CONCAT('https://freshbox-test1.s3.ap-southeast-3.amazonaws.com/photo_item/', e.photo) as photo
        FROM master_item_price a
        LEFT JOIN master_item b ON b.sku = a.sku
        LEFT JOIN master_item_category c ON c.id = b.item_category_id
        LEFT JOIN master_uom d ON d.id = b.uom_id
        LEFT JOIN master_item_gallery e ON e.sku = a.sku
        WHERE a.period_end > current_date()
        AND e.is_main = 1
        AND a.group_price_id = (SELECT y.client_group_price_id FROM users x, master_client y WHERE x.pos_client_no = y.no and x.id = 1276)
        and c.name = '$category_name'";

        return DB::select($sql);
    }

    public static function getProductBySearch($search, $user_id) {
        $sql = "SELECT a.sku,
        b.name,
        d.name as uom_name,
        b.description,
        b.item_category_id,
        c.name as category_name,
                (SELECT x.price
                 FROM master_item_price x
                 WHERE x.period_start <= current_date() and x.period_end >= current_date()
                     AND x.group_price_id = a.group_price_id
                     AND x.sku = a.sku
                 ORDER BY updated_at DESC
                 LIMIT 1) as unit_price,
        CONCAT('https://freshbox-test1.s3.ap-southeast-3.amazonaws.com/photo_item/', e.photo) as photo
        FROM master_item_price a
        LEFT JOIN master_item b ON b.sku = a.sku
        LEFT JOIN master_item_category c ON c.id = b.item_category_id
        LEFT JOIN master_uom d ON d.id = b.uom_id
        LEFT JOIN master_item_gallery e ON e.sku = a.sku
        WHERE a.period_end > current_date()
        AND e.is_main = 1
        AND a.group_price_id = (SELECT y.client_group_price_id FROM users x, master_client y WHERE x.pos_client_no = y.no and x.id = 1276)
        and b.name like '%$search%'";

        return DB::select($sql);
    }

    public static function getCategory() {
        $sql = "SELECT id, name, photo FROM master_item_category WHERE is_selling = 1";

        return DB::select($sql);
    }

    public static function getOutstanding($list_channel_id, $start_date, $end_date, $depo_source_id = null, $depo_delivery_id = null, $client_no = null)
    {
        $whereConditions = "";

        if(!empty($depo_source_id)) {
            $whereConditions .= "AND c.depo_id IN ($depo_source_id) ";
        }

        if(!empty($depo_delivery_id)) {
            $whereConditions .= " AND b.depo_id_delivery IN ($depo_delivery_id) ";
        }

        if(!empty($client_no)) {
            $whereConditions .= " AND b.client_no = '$client_no' ";
        }

        $sql = "SELECT b.branch_id,  b.branch_name, b.channel_id , b.channel_name,
                       b.depo_id_delivery AS depo_delivery_id, b.depo_name_delivery AS depo_delivery_name,
                       c.depo_id as depo_source_id, d.name as depo_source_name, b.client_no, b.client_name, b.fulfill_date, e.route_no,
                       count(a.sku) as total_sku_order, count(a.qty_qc) as total_sku_qc,  (count(a.sku) - count(a.qty_qc) ) as total_sku_diff,
                       sum(a.qty_order) as total_qty_order, IFNULL(sum(a.qty_qc),0) as total_qty_qc, ( IFNULL(sum(a.qty_qc),0) - sum(a.qty_order) ) as total_qty_diff
                FROM trx_sales_detail a
                LEFT JOIN view_trx_sales_header b on b.no = a.sales_no
                LEFT JOIN view_trx_item_source c on c.branch_id = b.branch_id and c.sku = a.sku
                LEFT JOIN mst_depo d on d.id = c.depo_id
                LEFT JOIN mst_client_route e on e.client_no = b.client_no
                WHERE b.status = '3'
                  AND b.fulfill_date between '$start_date' AND '$end_date'
                  AND b.channel_id IN ($list_channel_id)
                  $whereConditions
                GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12
                HAVING (count(a.sku) - count(a.qty_qc) ) > 0";
        return DB::select($sql);
    }

    public static function getDone($list_channel_id, $start_date, $end_date, $depo_source_id, $depo_delivery_id)
    {
        $whereConditions = "";

        if(!empty($depo_source_id)) {
            $whereConditions .= "AND c.depo_id IN ($depo_source_id) ";
        }

        if(!empty($depo_delivery_id)) {
            $whereConditions .= " AND b.depo_id_delivery IN ($depo_delivery_id) ";
        }

        $sql = "SELECT b.branch_id,  b.branch_name, b.channel_id , b.channel_name,
                       b.depo_id_delivery AS depo_delivery_id, b.depo_name_delivery AS depo_delivery_name,
                       c.depo_id as depo_source_id, d.name as depo_source_name, b.client_no, b.client_name, e.route_no, b.fulfill_date,
                       count(a.sku) as total_sku_order, count(a.qty_qc) as total_sku_qc,  (count(a.sku) - count(a.qty_qc) ) as total_sku_diff,
                       sum(a.qty_order) as total_qty_order, IFNULL(sum(a.qty_qc),0) as total_qty_qc, ( IFNULL(sum(a.qty_qc),0) - sum(a.qty_order) ) as total_qty_diff
                FROM trx_sales_detail a
                LEFT JOIN view_trx_sales_header b on b.no = a.sales_no
                LEFT JOIN view_trx_item_source c on c.branch_id = b.branch_id and c.sku = a.sku
                LEFT JOIN mst_depo d on d.id = c.depo_id
                LEFT JOIN mst_client_route e on e.client_no = b.client_no
                WHERE b.status = '3'
                  AND b.fulfill_date between '$start_date' AND '$end_date'
                  AND b.channel_id IN ($list_channel_id)
                  $whereConditions
                GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12
                HAVING (count(a.sku) - count(a.qty_qc) ) = 0";
        return DB::select($sql);
    }

    public static function getSpesific($list_channel_id, $start_date, $end_date, $depo_source_id = null, $depo_delivery_id = null, $client_no = null, $sales_no = null)
    {
        $whereConditions = "";

        if(!empty($depo_source_id)) {
            $whereConditions .= "AND c.depo_id IN ($depo_source_id) ";
        }

        if(!empty($depo_delivery_id)) {
            $whereConditions .= " AND b.depo_id_delivery IN ($depo_delivery_id) ";
        }

        if(!empty($client_no)) {
            $whereConditions .= " AND b.client_no = '$client_no' ";
        }

        if(!empty($sales_no)) {
            $whereConditions .= " AND a.sales_no = '$sales_no' ";
        }

        $sql = "SELECT b.branch_id,  b.branch_name, b.channel_id , b.channel_name,
                       b.depo_id_delivery AS depo_delivery_id, b.depo_name_delivery AS depo_delivery_name,
                       c.depo_id as depo_source_id, d.name as depo_source_name, a.sales_no, b.client_no, b.client_name, b.fulfill_date,
                       count(a.sku) as total_sku_order, count(a.qty_qc) as total_sku_qc,  (count(a.sku) - count(a.qty_qc) ) as total_sku_diff,
                       sum(a.qty_order) as total_qty_order, IFNULL(sum(a.qty_qc),0) as total_qty_qc, ( IFNULL(sum(a.qty_qc),0) - sum(a.qty_order) ) as total_qty_diff
                FROM trx_sales_detail a
                LEFT JOIN view_trx_sales_header b on b.no = a.sales_no
                LEFT JOIN view_trx_item_source c on c.branch_id = b.branch_id and c.sku = a.sku
                LEFT JOIN mst_depo d on d.id = c.depo_id
                WHERE b.status = '3'
                  AND b.fulfill_date between '$start_date' AND '$end_date'
                  AND b.channel_id IN ($list_channel_id)
                  $whereConditions
                GROUP BY 1,2,3,4,5,6,7,8,9,10,11";
        return DB::select($sql);
    }

    public static function getDetail($client_no, $fulfill_date, $depo_source_id, $depo_delivery_id, $sales_no = null, $sku = null)
    {
        $whereConditions = "";

        if(!empty($sales_no)) {
            $whereConditions .= "AND a.sales_no = '$sales_no' ";
        }

        if(!empty($sku)) {
            $whereConditions .= "AND a.sku = $sku ";
        }

        $sql = "SELECT  a.id, a.sales_no,
                        a.sku, e.name as sku_name, a.sku_note, e.uom_id, f.name as uom_name,
                        a.unit_price, a.qty_order, a.qty_qc, ifnull(a.qty_package,0) as qty_package, (ifnull(a.qty_qc, 0) - a.qty_order) as total_qty_diff,
                        a.qty_confirm, a.total_price, a.discount_percent, a.discount_amount, a.total_amount,
                        a.tax_percent, a.tax_amount, a.total_final,
                        a.created_by, a.updated_by, a.qc_by, null as qc_note,
                        a.created_at, a.updated_at, a.qc_at,
                        a.*
                FROM trx_sales_detail a
                LEFT JOIN view_trx_sales_header b on b.no = a.sales_no
                LEFT JOIN view_trx_item_source c on c.branch_id = b.branch_id and c.sku = a.sku
                LEFT JOIN mst_depo d on d.id = c.depo_id
                LEFT JOIN mst_item e on e.sku = a.sku
                LEFT JOIN mst_uom f on f.id = e.uom_id
                WHERE b.status = '3'
                  AND b.fulfill_date = '$fulfill_date'
                  AND b.client_no = '$client_no'
                  AND b.depo_id_delivery IN ($depo_delivery_id)
                  AND c.depo_id IN ($depo_source_id)
                  $whereConditions
                ORDER BY a.qc_at ASC";
        return DB::select($sql);
    }

    public static function update($id, $qty_qc = null, $qty_package = null, $qc_note = null, $updated_by) {
        try {
            $whereConditions = "";

            if(isset($qty_qc)) {
                $removeQtyQcDot = str_replace(".", "", $qty_qc);
                $replaceQtyQcComma = str_replace(",", ".", $removeQtyQcDot);
                $qtyQc = $replaceQtyQcComma;

                $whereConditions .= ", qty_qc = $qtyQc ";
            }

            if(!empty($qty_package)) {
                $whereConditions .= ", qty_package = '$qty_package' ";
            }

            if(!empty($qc_note)) {
                $whereConditions .= ", qc_note = '$qc_note' ";
            }

            $sqlUpdate = "UPDATE trx_sales_detail
                          SET qc_at = CURRENT_TIMESTAMP(),
                              qc_by = $updated_by
                              $whereConditions
                          WHERE id = $id";

            DB::beginTransaction();
            DB::update($sqlUpdate);
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("QcChecklistService::update Error: " . $e->getMessage() . " - StackTrace: " . $e->getTraceAsString());
            throw $e;
        }
    }

    //checkPayment
    public static function checkPayment($sales_no) {
        try {
            $sql = "SELECT sum(amount) as payment FROM pos_sales_payment
            WHERE sales_no = '$sales_no'";

            return DB::select($sql);
        } catch (\Exception $e) {
            Log::error("QcChecklistService::checkPayment Error: " . $e->getMessage() . " - StackTrace: " . $e->getTraceAsString());
            throw $e;
        }
    }

    //checkSales
    public static function checkSales($sales_no) {
        try {
            $sql = "SELECT total FROM sales WHERE `no` = '$sales_no'";

            return DB::select($sql);
        } catch (\Exception $e) {
            Log::error("QcChecklistService::checkSales Error: " . $e->getMessage() . " - StackTrace: " . $e->getTraceAsString());
            throw $e;
        }
    }

    //checkPaymentTypeCashout
    public static function checkPaymentTypeCashout($sales_no) {
        try {
            $sql = "SELECT * FROM pos_sales_payment
            WHERE sales_no = '$sales_no'
            AND payment_type_id = 10";

            return DB::select($sql);
        } catch (\Exception $e) {
            Log::error("QcChecklistService::checkPaymentType Error: " . $e->getMessage() . " - StackTrace: " . $e->getTraceAsString());
            throw $e;
        }
    }

    //updatePaymentType
    public static function updatePaymentType($sales_no, $amount, $user_id) {
        try {
            $sql = "UPDATE pos_sales_payment
            SET amount = $amount,
                updated_by = $user_id,
                updated_at = CURRENT_TIMESTAMP()
            WHERE sales_no = '$sales_no'
            AND payment_type_id = 10";

            DB::beginTransaction();
            DB::update($sql);
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("QcChecklistService::updatePaymentType Error: " . $e->getMessage() . " - StackTrace: " . $e->getTraceAsString());
            throw $e;
        }
    }

    //createPaymentType
    public static function createPaymentType($sales_no, $amount, $user_id) {
        try {
            $sql = "INSERT INTO pos_sales_payment
            (sales_no, payment_type_id, amount, created_by, created_at)
            VALUES
            ('$sales_no', 10, $amount, $user_id, CURRENT_TIMESTAMP())";

            DB::beginTransaction();
            DB::insert($sql);
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("QcChecklistService::createPaymentType Error: " . $e->getMessage() . " - StackTrace: " . $e->getTraceAsString());
            throw $e;
        }
    }

    //hasDetail
    public static function hasDetail($sales_no, $sku) {
        try {
            $sql = "SELECT * FROM sales_detail
            WHERE sales_no = '$sales_no'
            AND sku = '$sku'";

            return DB::select($sql);
        } catch (\Exception $e) {
            Log::error("QcChecklistService::hasDetail Error: " . $e->getMessage() . " - StackTrace: " . $e->getTraceAsString());
            throw $e;
        }
    }

    //updateSalesDetail
    public static function updateSalesDetail($sales_no, $sku, $discount_percent = null, $discount_amount= null) {
        try {
            $whereConditions = "";

            if(!empty($discount_percent)) {
                $whereConditions .= ", discount_percent = $discount_percent ";
            }

            if(!empty($discount_amount)) {
                $whereConditions .= ", discount_amount = $discount_amount ";
            }

            $sql = "UPDATE sales_detail
            SET updated_at = CURRENT_TIMESTAMP()
            $whereConditions
            WHERE sales_no = '$sales_no'
            AND sku = '$sku'";

            //recalculate total_final
            $sqlUpdate = "UPDATE sales_detail
            SET total_final = (total_price - discount_amount) + tax_amount
            WHERE sales_no = '$sales_no'
            AND sku = '$sku'";

            DB::beginTransaction();
            DB::update($sql);
            DB::update($sqlUpdate);
            DB::commit();

            self::recalculateHeader($sales_no);
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("QcChecklistService::updateSalesDetail Error: " . $e->getMessage() . " - StackTrace: " . $e->getTraceAsString());
            throw $e;
        }
    }

    //recalculate header
    public static function recalculateHeader($sales_no) {
        try {
            $sql = "UPDATE sales
            SET total = (SELECT sum(total_final) FROM sales_detail WHERE sales_no = '$sales_no')
            WHERE `no` = '$sales_no'";

            DB::beginTransaction();
            DB::update($sql);
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("QcChecklistService::recalculateHeader Error: " . $e->getMessage() . " - StackTrace: " . $e->getTraceAsString());
            throw $e;
        }
    }

    //deletePayment
    public static function deletePayment($sales_no, $payment_type) {
        try {
            $sql = "DELETE FROM pos_sales_payment
            WHERE sales_no = '$sales_no'
            AND payment_type_id = $payment_type";

            DB::beginTransaction();
            DB::delete($sql);
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("QcChecklistService::deletePayment Error: " . $e->getMessage() . " - StackTrace: " . $e->getTraceAsString());
            throw $e;
        }
    }

}
