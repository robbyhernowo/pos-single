<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pos_sales_payment extends Model
{
    use HasFactory;

    protected $table = 'pos_sales_payment';

    protected $fillable = [
        'sales_no',
        'payment_type_id',
        'reference_no',
        'amount',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];
}
