<?php

use App\Http\Controllers\PosOrder;
use App\Http\Controllers\ProfileController;
use App\Livewire\PosPrint;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
    return view('pos.pages.login');
});

Route::get('/pos-login', function () {
    return view('pos.pages.login');
});


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');


    // Route::get('/pos-order', function () {
    //     return view('pos.pages.test');
    // });

    Route::get('/pos-order', [PosOrder::class, 'index'])->name('posOrder');

    // Route::get('/print/{newOrder}', function () {
    //     return view('pos.pages.print');
    // });
    Route::get('/print/{newOrder}', PosPrint::class)->name('posPrint');
});

require __DIR__.'/auth.php';
