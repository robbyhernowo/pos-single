<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Fastkart">
    <meta name="keywords" content="Fastkart">
    <meta name="author" content="Fastkart">
    <link rel="icon" href="{{ asset('ecommerce/assets/images/favicon/4.png') }}" type="image/x-icon">
    <title>Freshbox Ecommerce</title>

    <!-- Google font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Russo+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <!-- bootstrap css -->
    <link id="rtl-link" rel="stylesheet" type="text/css"
        href="{{ asset('ecommerce/assets/css/vendors/bootstrap.css') }}">

    <!-- wow css -->
    <link rel="stylesheet" href="{{ asset('ecommerce/assets/css/animate.min.css') }}" />

    <!-- font-awesome css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/vendors/font-awesome.css') }}">

    <!-- feather icon css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/vendors/feather-icon.css') }}">

    <!-- slick css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/vendors/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/vendors/slick/slick-theme.css') }}">

    <!-- Iconly css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/bulk-style.css') }}">

    <!-- Template css -->
    <link id="color-link" rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/style.css') }}">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/vendors/bootstrap.css') }}">

    <!-- Theme css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/style.css') }}">
    @livewireStyles()
</head>

<body class="theme-color-2 bg-effect">

    <!-- Loader Start -->
    {{-- <div class="fullpage-loader">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div> --}}
    <!-- Loader End -->




    @yield('content')

    {{-- hero --}}
    {{-- @include('components.hero') --}}

    {{-- category --}}
    {{-- @include('components.category') --}}


    {{-- value --}}
    {{-- @include('components.value') --}}

    {{-- deal --}}
    {{-- @include('components.deal') --}}

    {{-- product --}}
    {{-- @include('components.products') --}}

    {{-- testimonial --}}

    {{-- @include('components.productSection') --}}

    {{-- @include('components.banner2') --}}


    {{-- @include('components.productsTop') --}}



    {{-- @include('components.blogs') --}}


    {{-- @include('components.newsLetter') --}}








    <!-- Cookie Bar Box Start -->
    {{-- <div class="cookie-bar-box">
        <div class="cookie-box">
            <div class="cookie-image">
                <img src="{{ asset('ecommerce/assets/images/cookie-bar.png') }}" class="blur-up lazyload"
                    alt="">
                <h2>Cookies!</h2>
            </div>

            <div class="cookie-contain">
                <h5 class="text-content">We use cookies to make your experience better</h5>
            </div>
        </div>

        <div class="button-group">
            <button class="btn privacy-button">Privacy Policy</button>
            <button class="btn ok-button">OK</button>
        </div>
    </div> --}}
    <!-- Cookie Bar Box End -->



    <!-- Tap to top start -->
    {{-- <div class="theme-option">
        <div class="back-to-top">
            <a id="back-to-top" href="#">
                <i class="fas fa-chevron-up"></i>
            </a>
        </div>
    </div> --}}
    <!-- Tap to top end -->

    <!-- Bg overlay Start -->
    {{-- <div class="bg-overlay"></div> --}}
    <!-- Bg overlay End -->



    <!-- latest jquery-->
    <script src="{{ asset('ecommerce/assets/js/jquery-3.6.0.min.js') }}"></script>

    <!-- jquery ui-->
    <script src="{{ asset('ecommerce/assets/js/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap js-->
    <script src="{{ asset('ecommerce/assets/js/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('ecommerce/assets/js/bootstrap/popper.min.js') }}"></script>

    <!-- feather icon js-->
    <script src="{{ asset('ecommerce/assets/js/feather/feather.min.js') }}"></script>
    <script src="{{ asset('ecommerce/assets/js/feather/feather-icon.js') }}"></script>

    <!-- Lazyload Js -->
    <script src="{{ asset('ecommerce/assets/js/lazysizes.min.js') }}"></script>

    <!-- Slick js-->
    <script src="{{ asset('ecommerce/assets/js/slick/slick.js') }}"></script>
    <script src="{{ asset('ecommerce/assets/js/bootstrap/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('ecommerce/assets/js/slick/custom_slick.js') }}"></script>

    <!-- Auto Height Js -->
    <script src="{{ asset('ecommerce/assets/js/auto-height.js') }}"></script>

    <!-- Quantity Js -->
    <script src="{{ asset('ecommerce/assets/js/quantity.js') }}"></script>

    <!-- sidebar open js -->
    <script src="{{ asset('ecommerce/assets/js/filter-sidebar.js') }}"></script>

    <!-- Timer Js -->
    <script src="{{ asset('ecommerce/assets/js/timer1.js') }}"></script>
    <script src="{{ asset('ecommerce/assets/js/timer2.js') }}"></script>
    <script src="{{ asset('ecommerce/assets/js/timer3.js') }}"></script>
    <script src="{{ asset('ecommerce/assets/js/timer4.js') }}"></script>

    <!-- Fly Cart Js -->
    <script src="{{ asset('ecommerce/assets/js/fly-cart.js') }}"></script>

    <!-- WOW js -->
    <script src="{{ asset('ecommerce/assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('ecommerce/assets/js/custom-wow.js') }}"></script>

    <!-- script js -->
    <script src="{{ asset('ecommerce/assets/js/script.js') }}"></script>

    <!-- Lordicon Js -->
    <script src="{{ asset('ecommerce/assets/js/lusqsztk.js') }}"></script>


    {{-- --------------------- --}}

    <!-- Delivery Option js -->
    <script src="{{ asset('ecommerce/assets/js/delivery-option.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>

    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/vendors/font-awesome.css') }}">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/vendors/bootstrap.css') }}">

    <!-- Theme css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ecommerce/assets/css/style.css') }}">

    {{-- Custom Script --}}
    @yield('js')

    @livewireScripts()
</body>

</html>
