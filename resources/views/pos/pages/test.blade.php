@extends('pos.layout.default', [
    'bodyClass' => 'pace-top',
    'appHeaderHide' => true,
    'appSidebarHide' => true,
    'appClass' => 'app-content-full-height app-without-sidebar app-without-header',
    'appContentClass' => 'p-1 ps-xl-4 pe-xl-4 pt-xl-3 pb-xl-3',
])

@section('title', 'Customer Order')

@push('js')
    <script src="/assets/js/demo/pos-customer-order.demo.js"></script>
    <script>
        document.addEventListener('livewire:load', function() {
            Livewire.on('openPosProductModal', () => {
                $('#posProductModal').modal('show');
            });
        });
    </script>
    {{-- <script>
        $(document).ready(function() {
            @if (session('openInNewTab'))
                window.open("{{ route('posPrint', ['newOrder' => $this->newOrder]) }}", '_blank');
            @endif
        });
    </script> --}}
@endpush

@section('content')
    <!-- BEGIN pos -->
    <div class="pos card" id="pos">
        <div class="pos-container card-body">
            <!-- BEGIN pos-menu -->
            <livewire:pos-category />
            <!-- END pos-menu -->

            <!-- BEGIN pos-content -->
            <livewire:pos-product />
            <!-- END pos-content -->

            <!-- BEGIN pos-sidebar -->
            <livewire:pos-sidebar />
            <!-- END pos-sidebar -->
        </div>
        <div class="card-arrow">
            <div class="card-arrow-top-left"></div>
            <div class="card-arrow-top-right"></div>
            <div class="card-arrow-bottom-left"></div>
            <div class="card-arrow-bottom-right"></div>
        </div>
    </div>
    <!-- END pos -->

    <!-- BEGIN pos-mobile-sidebar-toggler -->
    <a href="#" class="pos-mobile-sidebar-toggler" data-toggle-class="pos-mobile-sidebar-toggled"
        data-toggle-target="#pos">
        <i class="bi bi-bag"></i>
        <span class="badge">5</span>
    </a>
    <!-- END pos-mobile-sidebar-toggler -->
    <livewire:pos-product-modal />

    <!-- BEGIN #modalPosItem -->
    {{-- @include('livewire.pos-product-modal') --}}
    <!-- END #modalPosItem -->
@endsection
