@extends('pos.layout.default', [
    'bodyClass' => 'pace-top',
    'appHeaderHide' => true,
    'appSidebarHide' => true,
    'appClass' => 'app-content-full-height app-without-sidebar app-without-header',
    'appContentClass' => 'p-1 ps-xl-4 pe-xl-4 pt-xl-3 pb-xl-3',
])

@section('title', 'Customer Order')

@push('js')
    <script src="/assets/js/demo/pos-customer-order.demo.js"></script>
@endpush

@section('content')
    <livewire:pos-print />
@endsection
