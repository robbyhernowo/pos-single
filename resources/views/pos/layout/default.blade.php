<!DOCTYPE html>
<html data-bs-theme="dark" lang="{{ app()->getLocale() }}"{{ !empty($htmlAttribute) ? $htmlAttribute : '' }}>

<head>
    <meta charset="utf-8">
    <title>Freshbox | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('metaDescription')">
    <meta name="author" content="@yield('metaAuthor')">
    <meta name="keywords" content="@yield('metaKeywords')">

    @stack('metaTag')

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="/assets/css/vendor.min.css" rel="stylesheet">
    <link href="/assets/css/app.min.css" rel="stylesheet">
    <!-- ================== END BASE CSS STYLE ================== -->

    @stack('css')
    @livewireStyles()
</head>

<body class="{{ !empty($bodyClass) ? $bodyClass : '' }}">
    <!-- BEGIN #app -->
    <div id="app" class="app {{ !empty($appClass) ? $appClass : '' }}">
        @includeWhen(empty($appHeaderHide), 'pos.partial.header')
        @includeWhen(empty($appSidebarHide), 'pos.partial.sidebar')
        @includeWhen(!empty($appTopNav), 'pos.partial.top-nav')

        @if (empty($appContentHide))
            <!-- BEGIN #content -->
            <div id="content" class="app-content  {{ !empty($appContentClass) ? $appContentClass : '' }}">
                @yield('content')
            </div>
            <!-- END #content -->
        @else
            @yield('content')
        @endif

        @includeWhen(!empty($appFooter), 'partial.footer')
    </div>
    <!-- END #app -->

    @yield('outter_content')
    @include('pos.partial.scroll-top-btn')
    @include('pos.partial.theme-panel')
    @include('pos.partial.scripts')

    @livewireScripts()
</body>

</html>
