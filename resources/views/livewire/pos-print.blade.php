<div>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="style.css">
        <title>Freshbox Receipt</title>
    </head>

    <body>
        <div class="ticket">
            <img class="centered" src="{{ asset('ecommerce/assets/images/favicon/logofb.png') }}" alt="Logo">
            <p class="centered mb-1">
                <b>{{ env('MOI_PLACE') }}</b>
            </p>
            <p style="font-size: 10px" class="centered">
                <br>{{ $salesorder[0]->created_at }}
            </p>
            <div class="row mt-1">
                <div class="col-md-6">
                    <span style="text-align: left;font-size: 7px; float: left">Kasir : {{ Auth::user()->name }}</span>
                    <span style="text-align: right;font-size: 7px; float: right">{{ $salesorder[0]->no }}</span>
                </div>
            </div>
            <table>
                <thead>
                    <tr>
                        <th class="quantity">Q.</th>
                        <th class="description">Nama</th>
                        <th style="text-align: right" class="price">Total</th>
                    </tr>

                </thead>
                <tbody>
                    @forelse ($salesorderDetail as $detail)
                        <tr>
                            <td class="quantity"> {{ $detail->qty_order }}</td>
                            <td class="description">{{ $detail->name }}
                                <br>
                                @if ($detail->discount_amount != '0')
                                    <span style="font-size: 8px">Harga :
                                        Rp.{{ number_format($detail->total_price, 0, ',', '.') }}</span>
                                    <span style="font-size: 8px">Diskon :
                                        Rp.{{ number_format($detail->discount_amount, 0, ',', '.') }}</span>
                                @elseif ($detail->discount_percent != '0')
                                    <span style="font-size: 8px">Harga :
                                        Rp.{{ number_format($detail->total_price, 0, ',', '.') }}</span>
                                    <span style="font-size: 8px">Diskon :
                                        {{ number_format($detail->discount_percent, 0, ',', '.') }}%</span>
                                @endif
                            </td>
                            <td style="text-align: right" class="price">
                                {{ number_format($detail->total_final, 0, ',', '.') }}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3" class="text-center">
                                Data Kosong
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            <table style="display: block">
                <thead>
                    <tr>
                        <th style="text-align: left" class="description">TOTAL</th>
                        <th></th>
                        <th style="text-align: right" class="price"></th>
                        <th style="text-align: right" class="price">
                            {{ number_format($salesorder[0]->total, 0, ',', '.') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($postPayment as $payment)
                        <tr>
                            <td style="text-align: left" class="description">{{ $payment->name }}</td>
                            <td></td>
                            <td style="text-align: right" class="price"></td>
                            <td style="text-align: right" class="price">
                                {{ number_format($payment->amount, 0, ',', '.') }}</td>
                        </tr>
                    @endforeach
                    @if ($sumPayment <= 0)
                        <tr>
                            <td class="description">CHANGE</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right" class="price">
                                {{ number_format($sumPayment * -1, 0, ',', '.') }} </td>
                        </tr>
                    @else
                        <tr>
                            <td class="description">Sisa</td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right" class="price text-right">
                                {{ number_format($sumPayment * -1, 0, ',', '.') }}</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <br>
            <p class="centered">
                Terima kasih telah berbelanja di Freshbox
            </p>
            <p class="centered">

            </p>
        </div>
        {{-- <br>
        <br> --}}
        <button id="btnPrint" class="hidden-print">Print</button>
        <button id="backtoShop" class="hidden-print">Kembali belanja</button>
        {{-- button back to index --}}
        {{-- <a href="{{ route('landing') }}" class="btn btn-primary text-black">Back to Index</a> --}}
        <script src="script.js"></script>
    </body>

    </html>

    {{-- <script>
        const $btnPrint = document.querySelector("#btnPrint");
        $btnPrint.addEventListener("click", () => {
            window.print();
        });
    </script> --}}

    <script type="text/javascript">
        //menjalankan perintah cetak ketika halaman dimuat
        window.print();
        const $btnPrint = document.querySelector("#btnPrint");
        $btnPrint.addEventListener("click", () => {
            window.print();
        });

        // backtoShop route to landing
        const $backtoShop = document.querySelector("#backtoShop");
        $backtoShop.addEventListener("click", () => {
            window.location.href = "{{ route('posOrder') }}";
        });
    </script>

    <style>
        * {
            font-size: 12px;
            font-family: 'Times New Roman';
        }

        td,
        th,
        tr,
        table {
            border-top: 1px solid black;
            border-collapse: collapse;
        }

        /* tr solid class border solid */
        .solid {
            border-bottom: 1px solid black;
        }

        td.description,
        th.description {
            width: 75px;
            max-width: 75px;
        }

        td.quantity,
        th.quantity {
            width: 40px;
            max-width: 40px;
            word-break: break-all;
        }

        td.price,
        th.price {
            width: 60px;
            max-width: 60px;
            word-break: break-all;
        }

        .centered {
            text-align: center;
            align-content: center;
        }

        .ticket {
            width: 155px;
            max-width: 155px;
        }

        img {
            max-width: inherit;
            width: inherit;
        }

        /* display print class ticket */
        /* @media print {
            .ticket {
                display: block;
            }
        } */

        @media print {

            .hidden-print,
            .hidden-print * {
                display: none !important;
            }

            //hide back-to-top id
            #back-to-top {
                display: none;
            }

            //display only class ticket
            .ticket {
                display: block;
                page-break-inside: avoid
            }

            * {
                margin: 0;
                padding: 0;
            }
        }

        //only display print class ticket
        /* @media print {
            .ticket {
                display: block;
            }
        } */
    </style>

</div>
