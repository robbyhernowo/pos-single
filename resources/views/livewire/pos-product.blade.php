<div class="pos-content">
    <div class="pos-content-container h-100 p-4" data-scrollbar="true" data-height="100%">
        <div class="input-group mb-3">
            <input type="search" class="form-control" id="search" wire:model.live="search" placeholder="Search ..">
        </div>
        <div class="row gx-4">
            @forelse ($products as $product)
                <div class="col-xxl-3 col-xl-4 col-lg-6 col-md-4 col-sm-6 pb-4"
                    data-type="{{ $product->category_name }}">
                    <!-- BEGIN card -->
                    <div class="card h-100">
                        <div class="card-body h-100 p-1">
                            <a href="#" class="pos-product" wire:click="addItem({{ $product->sku }})">
                                {{-- <a href="#" class="pos-product" data-bs-toggle="modal" data-bs-target="#modalPosItem"> --}}
                                @if ($product->photo != null || $product->photo != '')
                                    <div class="img" style="background-image: url('{{ $product->photo }}')">
                                    </div>
                                @else
                                    <div class="img" style="background-image: url(/assets/img/pos/product-1.jpg)">
                                    </div>
                                @endif
                                <div class="info">
                                    <div class="title">{{ $product->name }}</div>
                                    {{-- <div class="title">Grill Chicken Chop&reg;</div> --}}
                                    <div class="desc">{{ $product->category_name }}</div>
                                    <div class="price">
                                        {{-- number format IDR --}}
                                        IDR {{ number_format($product->unit_price, 0, ',', '.') }}
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="card-arrow">
                            <div class="card-arrow-top-left"></div>
                            <div class="card-arrow-top-right"></div>
                            <div class="card-arrow-bottom-left"></div>
                            <div class="card-arrow-bottom-right"></div>
                        </div>
                    </div>
                    <!-- END card -->
                </div>
            @empty
            @endforelse
        </div>

    </div>
</div>
