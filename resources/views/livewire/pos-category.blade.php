<div class="pos-menu">
    <!-- BEGIN logo -->
    <div class="logo">
        <a href="/">
            <div class="logo-img"><i class="bi bi-gem" style="font-size: 2.1rem;"></i></div>
            <div class="logo-text">Freshbox</div>
        </a>
    </div>
    <!-- END logo -->
    <!-- BEGIN nav-container -->
    <div class="nav-container">
        <div data-scrollbar="true" data-height="100%" data-skip-mobile="true">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="#" data-filter="all">
                        <div class="card">
                            <div class="card-body">
                                <i class="fa fa-fw fa-utensils"></i> All Product
                            </div>
                            <div class="card-arrow">
                                <div class="card-arrow-top-left"></div>
                                <div class="card-arrow-top-right"></div>
                                <div class="card-arrow-bottom-left"></div>
                                <div class="card-arrow-bottom-right"></div>
                            </div>
                        </div>
                    </a>
                </li>
                @forelse ($categories as $category)
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-filter="{{ $category->name }}">
                            <div class="card">
                                <div class="card-body">
                                    <i class="fa-solid fa-fw fa-drumstick-bite fa-bounce fa-2xl"></i>
                                    <br>
                                    {{ $category->name }}
                                </div>
                                <div class="card-arrow">
                                    <div class="card-arrow-top-left"></div>
                                    <div class="card-arrow-top-right"></div>
                                    <div class="card-arrow-bottom-left"></div>
                                    <div class="card-arrow-bottom-right"></div>
                                </div>
                            </div>
                        </a>
                    </li>
                @empty
                @endforelse
                {{-- search input --}}
                {{-- <li class="nav-item">
                    <a class="nav-link" href="#" data-filter="burger">
                        <div class="card">
                            <div class="card-body">
                                <i class="fa fa-fw fa-hamburger"></i> Burger
                            </div>
                            <div class="card-arrow">
                                <div class="card-arrow-top-left"></div>
                                <div class="card-arrow-top-right"></div>
                                <div class="card-arrow-bottom-left"></div>
                                <div class="card-arrow-bottom-right"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-filter="pizza">
                        <div class="card">
                            <div class="card-body">
                                <i class="fa fa-fw fa-pizza-slice"></i> Pizza
                            </div>
                            <div class="card-arrow">
                                <div class="card-arrow-top-left"></div>
                                <div class="card-arrow-top-right"></div>
                                <div class="card-arrow-bottom-left"></div>
                                <div class="card-arrow-bottom-right"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-filter="drinks">
                        <div class="card">
                            <div class="card-body">
                                <i class="fa fa-fw fa-cocktail"></i> Drinks
                            </div>
                            <div class="card-arrow">
                                <div class="card-arrow-top-left"></div>
                                <div class="card-arrow-top-right"></div>
                                <div class="card-arrow-bottom-left"></div>
                                <div class="card-arrow-bottom-right"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-filter="desserts">
                        <div class="card">
                            <div class="card-body">
                                <i class="fa fa-fw fa-ice-cream"></i> Desserts
                            </div>
                            <div class="card-arrow">
                                <div class="card-arrow-top-left"></div>
                                <div class="card-arrow-top-right"></div>
                                <div class="card-arrow-bottom-left"></div>
                                <div class="card-arrow-bottom-right"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-filter="snacks">
                        <div class="card">
                            <div class="card-body">
                                <i class="fa fa-fw fa-cookie-bite"></i> Snacks
                            </div>
                            <div class="card-arrow">
                                <div class="card-arrow-top-left"></div>
                                <div class="card-arrow-top-right"></div>
                                <div class="card-arrow-bottom-left"></div>
                                <div class="card-arrow-bottom-right"></div>
                            </div>
                        </div>
                    </a>
                </li> --}}
            </ul>
        </div>
    </div>
    <!-- END nav-container -->
</div>
