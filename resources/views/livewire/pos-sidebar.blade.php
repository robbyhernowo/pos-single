<div class="pos-sidebar" id="pos-sidebar">
    <div class="h-100 d-flex flex-column p-0">
        <!-- BEGIN pos-sidebar-header -->
        <div class="pos-sidebar-header">
            <div class="back-btn">
                <button type="button" data-toggle-class="pos-mobile-sidebar-toggled" data-toggle-target="#pos"
                    class="btn">
                    <i class="bi bi-chevron-left"></i>
                </button>
            </div>
            <div class="icon"><img src="/assets/img/pos/icon-table-black.svg" class="invert-dark" alt="">
            </div>
            <div class="title">{{ Auth::user()->name }}</div>
            <div class="order">Order: <b>#0056</b></div>
        </div>
        <!-- END pos-sidebar-header -->

        <!-- BEGIN pos-sidebar-nav -->
        <div class="pos-sidebar-nav">
            <ul class="nav nav-tabs nav-fill">
                <li class="nav-item">
                    <a class="nav-link active" href="#" data-bs-toggle="tab" data-bs-target="#newOrderTab">New
                        Order ({{ $cartsCount }})</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-bs-toggle="tab" data-bs-target="#orderHistoryTab">Order
                        History ({{ $orderHistoryCount }})</a>
                </li>
            </ul>
        </div>
        <!-- END pos-sidebar-nav -->

        <!-- BEGIN pos-sidebar-body -->
        <div class="pos-sidebar-body tab-content" data-scrollbar="true" data-height="100%">
            <!-- BEGIN #newOrderTab -->
            <div class="tab-pane fade h-100 show active" id="newOrderTab">
                <!-- BEGIN pos-order -->
                @forelse ($carts as $cart)
                    <div class="pos-order">
                        <div class="pos-order-product">
                            <div class="img" style="background-image: url('{{ $cart->photo }}')">
                            </div>
                            <div class="flex-1">
                                <div class="h6 mb-1">{{ $cart->name }}</div>
                                <div class="small">Rp. {{ number_format($cart->unit_price, 0, ',', '.') }} x
                                    {{ number_format($cart->qty, 2, ',', '.') }}</div>
                                {{-- <div class="small mb-2">- size: large</div> --}}
                                <div class="d-flex">
                                    <a href="#" class="btn btn-outline-theme btn-sm"
                                        wire:click="decrementQuantity({{ $cart->sku }})"><i
                                            class="fa fa-minus"></i></a>
                                    <input
                                        class="form-control w-50px form-control-sm mx-2 bg-white bg-opacity-25 bg-white bg-opacity-25 text-center"
                                        type="text" name="quantity" id="qty-input"
                                        wire:model.defer="quantities.{{ $cart->sku }}"
                                        wire:change="updateItem('{{ $cart->sku }}')" :value="{{ $cart->qty }}" />
                                    <a href="#" class="btn btn-outline-theme btn-sm"
                                        wire:click="incrementQuantity({{ $cart->sku }})"><i
                                            class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="pos-order-price">
                            {{-- unit_price multiple with qty --}}
                            {{-- IDR {{ number_format($cart->unit_price, 0, ',', '.') }} --}}
                            Rp.{{ number_format(intval($cart->qty * intval($cart->unit_price)), 0, ',', '.') }}
                            <a href="#" class="btn btn-outline-danger btn-sm "
                                wire:click="removeItem({{ $cart->sku }})"><i class="bi bi-trash"></i></a>
                        </div>
                    </div>
                @empty
                @endforelse
            </div>
            <!-- END #orderHistoryTab -->

            <!-- BEGIN #orderHistoryTab -->
            <div class="tab-pane fade h-100" id="orderHistoryTab">

                @forelse ($orderHistory as $history)
                    <div class="pos-order">
                        <div class="pos-order-product">
                            <div class="flex-1">
                                <div class="h6 mb-1">{{ $history->invoice }}</div>
                                <div class="small">{{ $history->created_at }}</div>
                                <div class="small">
                                    {{-- {{ $history->status }} is status 1 Baru if 8 lunas --}}
                                    @if ($history->status == 1)
                                        <span class="badge bg-warning text-end">Baru</span>
                                    @elseif ($history->status == 2)
                                        <span class="badge bg-danger text-end">Batal</span>
                                    @elseif ($history->status == 8)
                                        <span class="badge bg-success text-end">Lunas</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="pos-order-price">
                            Rp. {{ number_format($history->total, 0, ',', '.') }}

                            {{-- button void & print --}}
                            <div class="d-flex">
                                <a href="#" class="btn btn-outline-danger btn-sm me-2"><i
                                        class="fa-solid fa-rectangle-xmark"></i></a>
                                <a class="btn btn-outline-default btn-sm"
                                    onclick="window.open('{{ route('posPrint', $history->invoice) }}', '_blank')"><i
                                        class="fa-solid
                                    fa-print"></i></a>
                                {{--                                 <button onclick="voidOrder({{ $history->id }})" data-bs-toggle="tooltip" title=""
                                    data-bs-original-title="Void" class="btn btn-outline-danger btn-sm me-2"><i
                                        class="bi bi-trash"></i></button>
                                <button onclick="printOrder({{ $history->id }})" data-bs-toggle="tooltip" title=""
                                    data-bs-original-title="Print" class="btn btn-outline-default btn-sm"><i
                                        class="bi bi-printer"></i></button> --}}
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="h-100 d-flex align-items-center justify-content-center text-center p-20">
                        <div>
                            <div class="mb-3 mt-n5">
                                <svg width="6em" height="6em" viewBox="0 0 16 16" class="text-gray-300"
                                    fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M14 5H2v9a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V5zM1 4v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4H1z" />
                                    <path d="M8 1.5A2.5 2.5 0 0 0 5.5 4h-1a3.5 3.5 0 1 1 7 0h-1A2.5 2.5 0 0 0 8 1.5z" />
                                </svg>
                            </div>
                            <h5>No order history found</h5>
                        </div>
                    </div>
                @endforelse
                <!-- END pos-order -->
            </div>
            <!-- END #orderHistoryTab -->
        </div>
        <!-- END pos-sidebar-body -->

        <!-- BEGIN pos-sidebar-footer -->
        <div class="pos-sidebar-footer">
            @if ($alert)
                {{-- <span class="error">{{ $alert['message'] }}</span> --}}
                <div class="alert alert-danger alert-dismissable fade show p-3">
                    <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                    <h5 class="alert-heading">{{ $alert['message'] }}</h5>
                </div>
            @endif
            <hr>
            <div class="d-flex align-items-center mb-2">
                <div>Total</div>
                <div class="flex-1 text-end h4 mb-0">Rp. {{ number_format($cartsTotal, 0, ',', '.') }}</div>
            </div>
            <div class="mt-3">
                <div class="btn-group d-flex">
                    {{-- <a href="#" class="btn btn-outline-default rounded-0 w-80px">
                        <i class="bi bi-bell fa-lg"></i><br>
                        <span class="small">Service</span>
                    </a>
                    <a href="#" class="btn btn-outline-default rounded-0 w-80px">
                        <i class="bi bi-receipt fa-fw fa-lg"></i><br>
                        <span class="small">Bill</span>
                    </a> --}}
                    <a href="#" wire:click.prevent="submitOrder()"
                        class="btn btn-outline-theme rounded-0 w-150px">
                        <i class="bi bi-send-check fa-lg"></i><br>
                        <span class="small">Submit Order</span>
                    </a>
                    {{-- <button wire:click="openModal()">Tampilkan Modal</button> --}}
                </div>
            </div>
        </div>
        <!-- END pos-sidebar-footer -->
    </div>
    {{-- modal --}}
    @if ($isOpen)
        <div class="modal modal-pos show" tabindex="-1" role="dialog" style="display: block;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content text-bg-dark">
                    <div class="modal-content border-0">
                        <div class="card">
                            <div class="card-body p-0">
                                <a href="#" wire:click="closeModal"
                                    class="btn-close position-absolute top-0 end-0 m-4"></a>
                                <div class="modal-pos-product">
                                    <div class="modal-pos-product-img">
                                        {{-- list payment --}}
                                        @foreach ($salesorderDetail as $detail)
                                            <div class="pos-order">
                                                <div class="pos-order-product">
                                                    <div class="flex-1">
                                                        <div class="h6 mb-1">{{ $detail->name }}</div>
                                                        <div class="small">Rp.
                                                            {{ number_format($detail->unit_price, 0, ',', '.') }} x
                                                            {{ number_format($detail->qty_order, 2, ',', '.') }}</div>
                                                    </div>
                                                </div>
                                                <div class="pos-order-price" style="width: 30%">
                                                    {{-- unit_price multiple with qty --}}
                                                    {{-- IDR {{ number_format($detail->unit_price, 0, ',', '.') }} --}}
                                                    {{-- Rp.{{ number_format($detail->total_price, 0, ',', '.') }} --}}
                                                    <select class="form-select form-select-sm"
                                                        wire:model.blur="discountTypes.{{ $detail->sku }}">
                                                        <option value="" selected>Pilih Discount</option>
                                                        <option value="percent">Percent</option>
                                                        <option value="amount">Amount</option>
                                                    </select>
                                                    {{-- <input class="form-control form-control-sm" type="text"
                                                        placeholder=".form-control-sm"> --}}
                                                    <input class="form-control form-control-sm" type="number"
                                                        name="discount" placeholder="Masukkan Angka"
                                                        wire:model.blur="discounts.{{ $detail->sku }}"
                                                        wire:change="updateDiscount('{{ $detail->sku }}')" />
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="modal-pos-product-info">
                                        <div class="h4 mb-2">{{ $newOrder }}</div>
                                        <div class="d-flex mb-3">
                                            {{-- <a href="#" class="btn btn-outline-theme"><i
                                                    class="fa fa-minus"></i></a> --}}
                                            {{-- <input type="text"
                                                class="form-control w-150px fw-bold mx-2 bg-inverse bg-opacity-15 border-0 text-center"
                                                name="qty" value="1"> --}}
                                            {{-- <a href="#" class="btn btn-outline-theme"><i
                                                    class="fa fa-plus"></i></a> --}}
                                            <div class="flex-1 h5 mb-0">Total Rp.
                                                {{ number_format($sumPayment, 0, ',', '.') }}</div>

                                        </div>
                                        <div class="mb-2">
                                            <div class="fw-bold">Payment: {{ $sendPaymentType }}</div>
                                            <div class="option-list">
                                                {{-- <div class="option">
                                                    <input type="radio" id="size3" name="size"
                                                        class="option-input" checked>
                                                    <label class="option-label" for="size3">
                                                        <span class="option-text">Small</span>
                                                        <span class="option-price">+0.00</span>
                                                    </label>
                                                </div> --}}
                                                @foreach ($paymentType as $item)
                                                    <div class="option">
                                                        <input type="radio" id="{{ $item->id }}"
                                                            value="{{ $item->id }}"
                                                            wire:model.live="sendPaymentType" class="option-input">
                                                        <label class="option-label" for="{{ $item->id }}">
                                                            <span class="option-text">{{ $item->name }}</span>
                                                            {{-- <span class="option-price">+3.00</span> --}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="mb-2">
                                            <div class="fw-bold">
                                                <button wire:click.prevent="addPayment({{ $i }})"
                                                    class="btn btn-outline-success"><i class="fa fa-plus fa-fw"></i>
                                                    Manual Payment</button>
                                            </div>
                                            {{-- <div class="option-list">
                                                <div class="option">
                                                    <input type="checkbox" name="addon[sos]" value="true"
                                                        class="option-input" id="addon1">
                                                    <label class="option-label" for="addon1">
                                                        <span class="option-text">More BBQ sos</span>
                                                        <span class="option-price">+0.00</span>
                                                    </label>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Jenis</th>
                                                    <th scope="col">Biaya</th>
                                                    <th scope="col"></th>
                                                </tr>
                                            </thead>
                                            @foreach ($inputs as $key => $value)
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <select class="form-select form-select-sm"
                                                                wire:model="payment_type_id_manual.{{ $key }}"
                                                                @error('payment_type_id_manual.{{ $key }}')
                                                                style="border-color: red"
                                                                @enderror>
                                                                <option value="">Pilih Jenis</option>
                                                                @foreach ($paymentType as $item)
                                                                    <option value="{{ $item->id }}">
                                                                        {{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control form-control-sm"
                                                                placeholder="Biaya"
                                                                wire:model="amount_manual.{{ $key }}"
                                                                wire:change="updatePaymentManual($event.target.value, {{ $key }})"
                                                                @error('amount_manual.{{ $key }}')
                                                                style="border-color: red"
                                                                @enderror>
                                                            @error('amount_manual.{{ $key }}')
                                                                <span
                                                                    class="text-red-500 text-xs">{{ $message }}</span>
                                                            @enderror
                                                        </td>
                                                        <td>
                                                            <button
                                                                wire:click.prevent="removePayment({{ $key }})"
                                                                class="btn btn-outline-danger btn-sm"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            @endforeach
                                        </table>


                                        {{-- if inputs has value  --}}
                                        @if ($inputs)
                                            <div class="fw-bold">
                                                {{-- <button wire:click.prevent="addPayment({{ $i }})"
                                                    class="btn btn-outline-success"><i class="fa fa-plus fa-fw"></i>
                                                    Hitung Payment</button> --}}

                                                <button type="button" class="btn btn-outline-lime"
                                                    wire:click.prevent="countManualPayment()"><i
                                                        class="fa-solid fa-calculator"></i> Hitung</button>
                                            </div>
                                        @endif


                                        <hr class="mx-n4">
                                        {{-- span error --}}

                                        <div class="row">
                                            <div class="col-4">
                                                <a href="#"
                                                    class="btn btn-default h4 mb-0 d-block rounded-0 py-3"
                                                    wire:click="closeModal">Cancel</a>
                                            </div>
                                            <div class="col-8">
                                                <a href="#"
                                                    class="btn btn-theme d-flex justify-content-center align-items-center rounded-0 py-3 h4 m-0"
                                                    {{-- wire:click.prevent="printOrder()" --}}
                                                    onclick="window.open('{{ route('posPrint', $salesorder[0]->no) }}', '_blank'); window.location.href='{{ route('posOrder') }}'">Print
                                                    <i
                                                        class="bi
                                                    bi-plus fa-2x ms-2 my-n3"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-arrow">
                                <div class="card-arrow-top-left"></div>
                                <div class="card-arrow-top-right"></div>
                                <div class="card-arrow-bottom-left"></div>
                                <div class="card-arrow-bottom-right"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-backdrop fade show"></div>
    @endif
</div>
